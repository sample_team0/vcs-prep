import gzip
import io
import json
import zlib
import base64


def lambda_handler(event, context):
    output = []
    for record in event['Records']:
        payload = base64.b64decode(record['kinesis']['data'])
        print(payload)

        output_record = {
            'result': 'Ok',
            'data': base64.b64encode(
                json.dumps(json.loads(payload)).encode('utf-8')).decode('utf-8')
        }
        print('---output record---')
        print(output_record)
        output.append(output_record)
    print('Successfully processed {} records.'.format(len(event['Records'])))
    return {'records': output}
