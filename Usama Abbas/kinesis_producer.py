# using link https://www.arundhaj.com/blog/getting-started-kinesis-python.html

import boto3
import json
from datetime import datetime
import calendar
import random
import time

kinesis_client = boto3.client('kinesis')


def put_to_stream(stream_name, payload, partition_key):

    put_response = kinesis_client.put_record(
                        StreamName=stream_name,
                        Data=json.dumps(payload),
                        PartitionKey=partition_key)
    return put_response


while True:

    property_value = random.randint(40, 120)
    property_timestamp = calendar.timegm(datetime.utcnow().timetuple())

    _stream_name = 'new-kinesis-stream'
    _partition_key = 'FSD'
    _payload = {
        'prop': str(property_value),
        'timestamp': str(property_timestamp),
        'partition_key': _partition_key
    }

    print('Payload to store: {0}'.format(_payload))
    response = put_to_stream(_stream_name, _payload, _partition_key)
    print('Response of put_to_stream: {0}'.format(response))

    # wait for 10 second
    time.sleep(10)
